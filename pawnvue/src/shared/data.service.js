// import * as axios from 'axios';

 import axios from "axios";

//  import VueAxios from 'vue-axios';

import { API } from './config';

const getPawns = async function() {
  try {
    // const response = await axios.get(`http://localhost:8888/pawns/`);
    const response = await axios.get(`${API}/pawns`);

    let data = parseList(response);

    const pawns = data;

    return pawns;
  } catch (error) {
    console.error(error);
    return [];
  }
};

const getPawn = async function(id) {
  try {
    console.log("Try to get pawn: " + id);
    const response = await axios.get(`${API}/pawns/${id}`);
    let pawn = parseItem(response, 200);
    return pawn;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const updatePawn = async function(pawn) {
  try {
    const response = await axios.put(`${API}/pawns/${pawn.id}`, pawn);
    const updatedPawn = parseItem(response, 200);
    return updatedPawn;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const addPawn = async function(pawn) {
  try {
    const response = await axios.post(`${API}/pawns/`, pawn);
    const addPawn = parseItem(response, 200);
    return addPawn;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const getSalesItems = async function() {
  try {
    const response = await axios.get(`${API}/salesitems`);

    let data = parseList(response);

    const salesItems = data;

    return salesItems;
  } catch (error) {
    console.error(error);
    return [];
  }
};

const getSalesItem = async function(id) {
  try {
    console.log("Try to get salesItem: " + id);
    const response = await axios.get(`${API}/salesitems/${id}`);
    let salesItem = parseItem(response, 200);
    return salesItem;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const updateSalesItem = async function(salesItem) {
  try {
    const response = await axios.put(`${API}/salesitems/${salesItem.id}`, salesItem);
    const updatedSalesItem = parseItem(response, 200);
    return updatedSalesItem;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const parseList = response => {
  if (response.status !== 200) throw Error(response.message);
  if (!response.data) return [];
  let list = response.data;
  if (typeof list !== 'object') {
    list = [];
  }
  return list;
};

export const parseItem = (response, code) => {
  if (response.status !== code) throw Error(response.message);
  let item = response.data;
  if (typeof item !== 'object') {
    item = undefined;
  }
  return item;
};

export const dataService = {
  getPawns,
  getPawn,
  updatePawn,
  getSalesItems,
  getSalesItem,
  updateSalesItem,
  addPawn

  
};
