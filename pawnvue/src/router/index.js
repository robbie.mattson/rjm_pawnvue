import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const parseProps = r => ({ id: parseInt(r.params.id) });

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/pawns',
    name: 'Pawns',
    component: () =>
      import(/* webpackChunkName: "bundle.pawns" */ '../views/pawns.vue'),
  },
  {
    path: '/pawns/:id',
    name: 'PawnDetail',
    // props: true,
    props: parseProps,
    component: () =>
      import(/* webpackChunkName: "bundle.pawns" */ '../views/pawn-detail.vue'),
  },
  {
    path: '/addpawn',
    name: 'AddPawn',
    // props: true,
    props: parseProps,
    component: () =>
      import(/* webpackChunkName: "bundle.pawns" */ '../views/addpawn.vue'),
  },
  {
    path: '/salesitems',
    name: 'SalesItems',
    component: () =>
      import(/* webpackChunkName: "bundle.salesItems" */ '../views/salesItems.vue'),
  },
  {
    path: '/salesitems/:id',
    name: 'SalesItemDetail',
    // props: true,
    props: parseProps,
    component: () =>
      import(/* webpackChunkName: "bundle.salesItems" */ '../views/salesItem-detail.vue'),
  }
  // ,
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
