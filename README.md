Robbie Mattson
Pawn Vue

In my time as a pawn broker, I realized that all of our pawn and customer information
was stored locally. I realized that if we had a web application, it would be simpler
to backup data, and for managers to check customer and employee info from different branches. 

So I made a pawn shop web application in a Vue framework, where users can log in and perform
pawn transactions. 

I learned quite a bit more about Vue, and it was a rewarding experience. 